import os
import uvicorn
from fastapi import FastAPI
from . import initdb
from .routers import router_v1
from .settings import HOST, PORT

os.system("cls")
app = FastAPI()
app.include_router(router_v1)


if __name__ == '__main__':
    init_status = initdb.init()
    if init_status != 'OK':
        print('ERROR:     Cannot connect to DB.')
        print(init_status)
    else:
        uvicorn.run(app=app, host=HOST, port=PORT)
