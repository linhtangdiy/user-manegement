from sqlalchemy import text
from sqlalchemy.exc import OperationalError
from app.modules.user import models
from app.database import engine


def check():
    try:
        engine.execute(text('select 1'))
    except OperationalError as e:
        return False, str(e)
    return True, None


def init():
    connected_db, error = check()
    if connected_db:
        models.Base.metadata.create_all(bind=engine)
        return 'OK'
    return error

