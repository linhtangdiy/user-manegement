import os
from dotenv import load_dotenv


dotenv_path = os.path.join(os.path.dirname(__file__), '../.env')
load_dotenv(os.path.abspath(dotenv_path))

POSTGRES_IP = os.getenv('POSTGRES_IP', '127.0.0.1')
POSTGRES_PORT = os.getenv('POSTGRES_PORT', 5432)
POSTGRES_USER = os.getenv('POSTGRES_USER', 'simpleapi')
POSTGRES_PASSWD = os.getenv('POSTGRES_PASSWD', '')
POSTGRES_DB = os.getenv('POSTGRES_DB', 'simpleapi')

HOST = os.getenv('HOST', '127.0.0.1')
PORT = int(os.getenv('PORT', 8000))

JWT_SECRET_KEY = os.getenv('JWT_SECRET_KEY', '')
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE = 60  # Minutes

PASSWD_MIN_CHARACTERS = 6
