from fastapi import APIRouter
from .modules.user.endpoints import login, signup, account

router_v1 = APIRouter(
    prefix="/v1",
    tags=["user"],
    responses={404: {"description": "Not found"}}
)

router_v1.include_router(login.router_v1)
router_v1.include_router(signup.router_v1)
router_v1.include_router(account.router_v1)
