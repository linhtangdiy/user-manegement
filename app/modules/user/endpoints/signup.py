import re

from fastapi import APIRouter, Form
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from ..schemas import UserCreate
from ..authen import get_password_hash
from ..crud import get_user_by_email, create_user
from ....dependencies import get_db
from ....settings import PASSWD_MIN_CHARACTERS

router_v1 = APIRouter()
regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'


@router_v1.post("/signup")
async def signup(email: str = Form(...), password: str = Form(...), db: Session = Depends(get_db)):
    if not re.fullmatch(regex, email):
        raise HTTPException(status_code=400, detail="Email is invalid")
    db_user = get_user_by_email(db, email=email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    if len(password) < PASSWD_MIN_CHARACTERS:
        raise HTTPException(status_code=400, detail=f"Password must be at least {PASSWD_MIN_CHARACTERS} characters")
    hashed_password = get_password_hash(password)
    user = UserCreate(email=email, hashed_password=hashed_password)
    return create_user(db=db, user=user)
