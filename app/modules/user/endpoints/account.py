from fastapi import APIRouter, Form, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.orm import Session
from jose import JWTError, jwt
from .. import schemas
from ..authen import verify_password, get_password_hash
from ..crud import get_user_by_id, update_profile, update_password
from ....dependencies import get_db
from ....settings import JWT_SECRET_KEY, ALGORITHM, PASSWD_MIN_CHARACTERS

v1_oauth2_scheme = OAuth2PasswordBearer(tokenUrl="v1/login")

router_v1 = APIRouter(
    prefix="/account",
    responses={404: {"description": "Not found"}}
)


@router_v1.post("/me", response_model=schemas.User)
async def get_current_user(db: Session = Depends(get_db),
                           token: str = Depends(v1_oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, JWT_SECRET_KEY, algorithms=[ALGORITHM])
        user_id: str = payload.get("sub")
        if user_id is None:
            raise credentials_exception
    except JWTError:
        raise credentials_exception
    user = get_user_by_id(db=db, user_id=user_id)
    if user is None:
        raise credentials_exception
    return user


@router_v1.put("/profile", response_model=schemas.User)
async def edit_profile(db: Session = Depends(get_db),
                       user: schemas.User = Depends(get_current_user),
                       first_name: str = Form(...), last_name: str = Form(...)):
    user.first_name = first_name
    user.last_name = last_name
    user_new = update_profile(db=db, user=user)
    return user_new


@router_v1.put("/password")
async def change_password(db: Session = Depends(get_db),
                          user: schemas.User = Depends(get_current_user),
                          old_password: str = Form(...), new_password: str = Form(...)):
    db_user = get_user_by_id(db=db, user_id=user.id)
    if verify_password(old_password, db_user.hashed_password):
        if len(new_password) < PASSWD_MIN_CHARACTERS:
            raise HTTPException(status_code=400,
                                detail=f"New password must be at least {PASSWD_MIN_CHARACTERS} characters")
        new_hashed_password = get_password_hash(new_password)
        update_password(db=db, user_id=user.id, hashed_password=new_hashed_password)
    else:
        raise HTTPException(status_code=401, detail=f"Old password incorrect")

    return "OK"
