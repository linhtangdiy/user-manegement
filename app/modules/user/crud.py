import uuid
from sqlalchemy.orm import Session
from . import models, schemas


def get_user_by_id(db: Session, user_id: str):
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_email(db: Session, email: str):
    return db.query(models.User).filter(models.User.email == email).first()


def create_user(db: Session, user: schemas.UserCreate):
    db_user = models.User(id=str(uuid.uuid1()), email=user.email, hashed_password=user.hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def update_profile(db: Session, user: schemas.User):
    db.query(models.User).filter(models.User.id == user.id)\
        .update(dict(first_name=user.first_name, last_name=user.last_name), synchronize_session="fetch")
    db.commit()
    return db.query(models.User).filter(models.User.id == user.id).first()


def update_password(db: Session, user_id: str, hashed_password: str):
    db.query(models.User).filter(models.User.id == user_id)\
        .update(dict(hashed_password=hashed_password), synchronize_session="fetch")
    db.commit()
